import '@babel/polyfill'
import Koa from 'koa'
import Router from 'koa-router'

const port = process.env.PORT || "3000"
const app = new Koa()
const router = new Router()

router.get("/v1/users", async ctx => ctx.body = [{
    id: 1,
    name: "Yo",
}, {
    id: 2,
    name: "Yea"
}])
router.get("/", async ctx => ctx.body = "Hello, Koa")

app.use(router.routes()).use(router.allowedMethods())
app.listen(port)

console.log(`Server running at http://localhost:${port}`)